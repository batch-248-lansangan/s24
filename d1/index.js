console.log("Hello World!");


//Exponent Operator
/*
const squared = 8 ** 2;

let message = 'Hello World!\n' + 'Welcome to programming!\n';

//Destructuring Arrays

let fullName = '${ name [0] } ${ name[1]} ${ name[2]}';

//Destructuring Object 
*/

//ES6 = ECMAScript is the standard that is used to create the implementations of language, one of which is JavaScript

//ES6 is one of the latest versions of writing JS and in fact is one of the major updates

	//let and const
	//are ES6 updates, these are new standards of creating variables
	
	//In JS, hoisting allows you to use functions and variables before they are declared
	//BUT this might cause confusion, because of the confusion that var hoisting can create, it is BEST to AVOID USING VARIABLES BEFORE THEY ARE DECLARED
	
	console.log(varSample);
	var varSample = "Hoist me up!!!";


	//if you have used "name" in other parts of the code, you might be surprised at the output we might get.
	var name = "Wing!";

	if(true){
		var name = "Hi!";
	}

	var name = "Win";

	console.log('My name is ' + name);

	//let is blocked scope

	let name1 = "Yong";

	if(true){
		let name1 = "Hello";
	} ;

	console.log(name1);

	//let and const

	const firstNum = 8**2;
	console.log(firstNum);

	const secondNum = Math.pow(8,2);
	console.log(secondNum);

	const eightPowerOf3 = 8**3;
	console.log(eightPowerOf3);

	let string1 = "fun";
	let string2 = "Bootcamp";
	let string3 = "Coding";
	let string4 = "JavaScript";
	let string5 = "Zuitt";
	let string6 = "Love";
	let string7 = "Learning";
	let string8 = "I";
	let string9 = "is";
	let string10 = "in";


	//"", '' - string literals

	//Template Literals
		//allows us to create strings using ``(backticks) and easily embed JS expressions in it
			/*
				it allows us to write strings without using concatenation operator (+)
				greatly helps with CODE READABILITY
	
			*/

	let sentence1 = `${string8} ${string6} ${string5} ${string3} ${string2}!`;
	console.log(sentence1);

	//${} is a placeholder that is used to embed JS expressions when creatign strings using Template Literals

	let name2 = "John";
	//("") or (''); //Pre-Template Literals

	let message = "Hello" + name2 + '! Welcome to programming';

	//Strings using template literals
	//(``) backticks

	message = `Hello ${name2}! Welcome to programming`;
	console.log(message);

	//Multi-line using Template Literals

	const anotherMessage = `



	${name2} attended a math competition. He won it by solving the problem 8**2 with the solution of ${firstNum}.

	`
	console.log(anotherMessage);

	let dev = {
		name: "Peter",
		lastName: "Parker",
		occupation: "Web developer",
		income: 50000,
		expense: 60000,
	};


	console.log(`${dev.name} is a ${dev.occupation}.`);
	console.log(`His income is ${dev.income} and expenses at ${dev.expenses}. His current balance is ${dev.income - dev.expenses}.`);

	const interestRate = .1;
	const principal = 1000;

	console.log(`The interest on ${dev.name}'s' savings account is ${principal * interestRate}.`);

	//Object Destructuring

	/*
		Allows us to unpack properties of objects into distinct variables

		Syntax 

		let/const {propertyNameA, propertyNameB, propertyNameC} = object

	*/


	const person = {
		givenName: "Jane",
		maidenName: "Dela",
		familyName: "Cruz"

	};

	//Pre-Object Destructuring

	//We can access them using . or []

	console.log(person.givenName);
	console.log(person.maidenName);
	console.log(person.familyName);

	//Object Destructuring

	const {givenName, maidenName, familyName } = person;

	console.log(givenName);
	console.log(maidenName);
	console.log(familyName);

	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

function getFullName ({ givenName, maidenName, familyName}) {
    console.log(`${ givenName } ${ maidenName } ${ familyName }`);
}

getFullName(person);


let pokemon1 = {

	namePkmn: "Bulbasaur",
	type: "Grass",
	level: 10,
	moves: ["Razor Leaf","Tackle","Leech Seed"]

}

let {level,type,namePkmn,moves,personality} = pokemon1;

console.log(level);
console.log(type);
console.log(namePkmn);
console.log(moves);
console.log(personality);//undefined

let pokemon2 = {

	namePkmn: "Charmander",
	type: "Fire",
	level: 11,
	moves: ["Ember","Scratch"]

}

//{propertyName: newVariable}
const {namePkmn: nameVar} = pokemon2;
console.log(nameVar);

console.log(`${nameVar} goes char char!`)


//Array Destructuring
	//allows us to unpack elements in arrays into distinct variables
	//allows us to name array elements with variables instead of using index numbers

	/*
		let/const [variableName, variableName, variableName] = array;

	*/

	const fullName = ["Juan", "Dela", "Cruz"];

	//pre-array destructuring
	console.log(fullName[0]);
	console.log(fullName[1]);
	console.log(fullName[2]);

	console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It is nice to meet you!`);


	//array destructuring
	const [firstName, middleName, lastName] = fullName;
	console.log(firstName);
	console.log(middleName);
	console.log(lastName);

	console.log(`Hello ${firstName} ${middleName} ${lastName}! It is nice to meet you again!`);


	let oneDirection = ["Harry", "Zayn", "Niall", "Louis", "Liam"];

	let [singer1, , singer3, singer4, singer5] = oneDirection;
	console.log(singer3);

	//Arrow Function

	/*
		an alternative way of writing functions in JS.

		Syntax:
		let/const variableName = (parameterA, parameterB, parameterC) => {
			console.log;
		}
	*/

	//Traditional Function

	function displayMessage(){
		console.log("Hello World!");
	}

	displayMessage();

	//Arrow Function

	const hello = () => {
		console.log("Hello from Arrow Function!");
	}

	hello();

	//Arrow function with Parameters

	const greet = (friend) => {
		console.log(`Hi ${friend}!`);
	}

	greet("Zayn");

	//Arrow vs Traditional Function

	//Implicit Return - this allows us to return a value from an arrow function without the use of return keyword

	//traditional addNum() function

	function addNum(num1, num2){
		let result = num1 + num2;
		return result
	}

	let sum = addNum(5,10);
	console.log(sum);

	let subNum = (num1, num2) => num1 - num2;
	let difference = subNum(10,5);
	console.log(difference);


	let addNo = (num1,num2) => num1 + num2;
	let sum1 = addNo(7,5);
	console.log(sum1);

	let subNo = (num1,num2) => num1 - num2;
	let difference1 = subNo(7,5);
	console.log(difference1);

	let multNo = (num1,num2) => num1 * num2;
	let product = multNo(7,5);
	console.log(product);

	let divideNo = (num1,num2) => num1 / num2;
	let quotient = divideNo(7,5);
	console.log(quotient);





// pre-arrow function

	const bears = ["Morgan", "Amy", "Lulu"];

	bears.forEach(function(bear){
		console.log(`${bear} is my bestfriend!`)
	})

	bears.forEach((bear)=>{
		console.log(`${bear} is my best buddy!`)
	})


	//Default Function Argument Value
	//provide a default argument value if none is provided when the function is invoked

	const messageForYou = (name = 'User') => {
		return `Good evening, ${name}. Hope you are okay =>`;
	}

	console.log(messageForYou());
	console.log(messageForYou("Zayn"));

	//Class-Based Object Blueprints

	/*
		Allows creation/instantiation of objects using classes as blueprints

		Syntax

		class Classname {
			constructor(objectPropertA, objectPropertB){
				this.objectPropertA = objectPropertA;
				this.objectPropertB = objectPropertB;
			}
		}

	*/
		class Car {
			constructor(brand,name,year){
				this.brand = brand;
				this.name = name;
				this.year = year;

			}
		}

		//Instantiating an object
		/*
			the "new" operator creates/instantiates a new object with the given arguments as the value of its properties
		*/

		//let/const variableName = new className();

		const myCar = new Car();
		console.log(myCar);

		myCar.brand = "Ford";
		myCar.name = "Everest";
		myCar.year = "1996";

		console.log(myCar);

		const myNewCar = new Car("Toyota", "Vios", 2000);
		console.log(myNewCar);


		class Character {
			constructor(name, role, strength, weakness){
				this.name = name;
				this.role = role;
				this.strength = strength;
				this.weakness = weakness;
				this.introduce = () => {
					console.log(`My hero name is ${this.name}! And I'm an S-Class hero.`)
				}

			}
		}

		const myCharacter = new Character();

		myCharacter.name = "Genos";
		myCharacter.role = "Fighter";
		myCharacter.strength = "Mech Abilities";
		myCharacter.weakness = "Saitama";

		console.log(myCharacter);
		myCharacter.introduce();

		const myNewCharacter = new Character("Puri-puri Prisoner", "Brawler", "Superhuman strength", "Chocolates");
		console.log(myNewCharacter);
		myNewCharacter.introduce();
