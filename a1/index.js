console.log("Hello World!");

/*

1. In the S24 folder, create an a1 folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.


3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)

// Template Literals
4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
5. Create a variable address with a value of an array containing details of an address.
6. Destructure the array and print out a message with the full address using Template Literals.

// Object Destructuring
7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
8. Destructure the object and print out a message with the details of the animal using Template Literals.

// Arrow Functions
9. Create an array of numbers.

// A
10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.

// B
11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

// Javascript Objects
12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
13. Create/instantiate a new object from the class Dog and console log the object.

14. Create a git repository named S24.
15. Initialize a local git repository, add the remote link and push to git with the commit message of "Add s24 activity code".
16. Add the link in Boodle.
*/

let num1 = 5;
const getCube = num1 ** 3;


console.log(`The cube of ${num1} is ${getCube}`);

let address = ["258 Washington Ave", "NW", "California", "90011"];

let [street, direction, state, zip] = address;

console.log(`I live at ${street} ${direction} ${state} ${zip}`);

const animal = {
		type: "monkey-eating eagle",
		name: "Lawin",
		weight: "23 kgs",
		length: "7 ft 2 in"

	};

	const {type, name, weight, length} = animal;

	console.log(`${name} was ${type}. He weighed at ${weight} with a wingspan of ${length}.`);





	const arrNums = [1, 2, 3, 4, 5] 
	arrNums.forEach((item) => console.log(item))

	const reduceNumber = arrNums.reduce((a, b) => a+b)
	console.log(reduceNumber)

	class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
		this.introduce = () => {
				console.log(`I'm ${this.name}! Gatekeeper of the Abyss.`)
				}
		}
	}




		const myDog = new Dog();

		myDog.name = "Cerberus";
		myDog.age = "65 Million years old";
		myDog.breed = "Hellhound";

		console.log(myDog);
		myDog.introduce();